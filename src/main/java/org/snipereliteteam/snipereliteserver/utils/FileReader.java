package org.snipereliteteam.snipereliteserver.utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileReader {

    private FileReader() {
    }


    public static String readFile(String path) throws IOException {
        String data = "";
        data = new String(Files.readAllBytes(Paths.get("src/main/resources/" + path)));
        return data;
    }

}
