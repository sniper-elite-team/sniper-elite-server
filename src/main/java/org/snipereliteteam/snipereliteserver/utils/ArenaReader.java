package org.snipereliteteam.snipereliteserver.utils;

import org.snipereliteteam.snipereliteserver.game.objects.*;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedList;
import java.util.List;

public class ArenaReader {

    private ArenaReader() {
    }


    public static List<GameObject> getObjects(String arena, List<Player> players) throws IOException {

        List<GameObject> objectsList = new LinkedList<>();

        objectsList.addAll(getObjectsByType(arena + "Walls", Wall.class));

        setPlayersPosition(arena + "Players", players);

        objectsList.addAll(players);

        return objectsList;
    }


    private static <T extends GameObject> List<GameObject> getObjectsByType(String type, Class<T> objectClass) throws IOException {

        String readedContent = FileReader.readFile(type);

        String[] gameObjects = readedContent.split("\n");

        List<GameObject> gameObjectList = new LinkedList<>();

        for (String gameObject : gameObjects) {

            String[] values = gameObject.split(";");

            int x = Integer.parseInt(values[0].substring(1));
            int y = Integer.parseInt(values[1]);
            int w = Integer.parseInt(values[2]);
            int h = Integer.parseInt(values[3].substring(0, values[3].indexOf("}")));

            Position position = new Position(x, y);

            try {
                GameObject object = objectClass.getDeclaredConstructor(Position.class, Integer.class, Integer.class).newInstance(position, w, h);
                gameObjectList.add(object);
            } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
                e.printStackTrace();
            }
        }
        return gameObjectList;

    }


    private static void setPlayersPosition(String type, List<Player> players) throws IOException {

        String readedContent = FileReader.readFile(type);

        String[] playersPosition = readedContent.split("\n");

        for (Player player : players) {
            String[] values = playersPosition[players.indexOf(player)].split(";");
            Position position = new Position(Integer.parseInt(values[0].substring(1)), Integer.parseInt(values[1].substring(0, values[1].indexOf("}"))));
            player.setPosition(position);

            player.setOrientation(OrientationEnum.N);
        }
    }

}
