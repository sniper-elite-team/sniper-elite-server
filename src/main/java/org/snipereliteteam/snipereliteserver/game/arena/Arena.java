package org.snipereliteteam.snipereliteserver.game.arena;

import org.snipereliteteam.snipereliteserver.game.objects.GameObject;
import org.snipereliteteam.snipereliteserver.game.objects.Player;
import org.snipereliteteam.snipereliteserver.game.objects.Projectile;

import java.io.IOException;
import java.util.List;

public interface Arena extends Runnable {

    void init(List<Player> players) throws IOException;

    void removePlayer(Player player);

    String getName();

    List<GameObject> getObjects();

    int getWIDTH();

    int getHEIGHT();

    int getMARGIN();

    void addProjectile(Projectile projectile);

}
