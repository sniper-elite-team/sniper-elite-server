package org.snipereliteteam.snipereliteserver.game.arena;

public enum ArenaEnum {
    MAZE(new Maze());

    private final Arena arena;


    ArenaEnum(Arena arena) {
        this.arena = arena;
    }


    public Arena getArena() {
        return arena;
    }

}
