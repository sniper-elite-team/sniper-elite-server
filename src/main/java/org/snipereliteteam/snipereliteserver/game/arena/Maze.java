package org.snipereliteteam.snipereliteserver.game.arena;

import org.snipereliteteam.snipereliteserver.game.objects.GameObject;
import org.snipereliteteam.snipereliteserver.game.objects.Player;
import org.snipereliteteam.snipereliteserver.game.objects.Projectile;
import org.snipereliteteam.snipereliteserver.handler.outgoing.CommandsSendEnum;
import org.snipereliteteam.snipereliteserver.utils.ArenaReader;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executors;

import static org.snipereliteteam.snipereliteserver.handler.outgoing.MessageDispatcher.sendSessionMessage;

public class Maze implements Arena {

    private List<GameObject> gameObjects;

    private List<Player> players;

    private final List<Projectile> projectiles = new CopyOnWriteArrayList<>();


    @Override
    public void init(List<Player> players) throws IOException {

        this.players = players;

        gameObjects = ArenaReader.getObjects(getName(), players);

        Executors.newSingleThreadExecutor().execute(this);

        sendSessionMessage(players, CommandsSendEnum.ARENA.getCommand() + getName());
        for (Player player : players) {
            sendSessionMessage(players,
                    CommandsSendEnum.ADD.getCommand() + player.getId() + "#" + player.getPosition().toString() + "#" + player.getOrientation().getOrientation());
        }
        sendSessionMessage(players, CommandsSendEnum.INIT.getCommand());
    }


    @Override
    public void run() {

        while (!hasEnded()) {

            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            for (Projectile projectile : projectiles) {
                projectile.move();
                if (projectile.hasCollision()) {
                    Player player = projectile.getPlayerCollision();
                    projectiles.remove(projectile);
                    if (player != null) {
                        sendSessionMessage(players, CommandsSendEnum.KILL.getCommand() + player.getId());
                        players.remove(player);
                    }
                    sendSessionMessage(players, CommandsSendEnum.SHOT_R.getCommand() + projectile.getId());
                }
            }

        }
        // Remove players from this and notify session
        System.out.println("Arena ended");
    }


    private boolean hasEnded() {
        return players.size() == 1;
    }


    @Override
    public void removePlayer(Player player) {
        gameObjects.remove(player);
    }


    @Override
    public String getName() {
        return "maze";
    }


    @Override
    public List<GameObject> getObjects() {
        return gameObjects;
    }


    @Override
    public void addProjectile(Projectile projectile) {
        projectiles.add(projectile);
        sendSessionMessage(players,
                CommandsSendEnum.SHOT.getCommand() + projectile.getId() + "#" + projectile.getPosition().toString() + "#" + projectile.getOrientation().getOrientation());

    }


    public int getWIDTH() {
        return 1600;
    }


    public int getHEIGHT() {
        return 1200;
    }


    public int getMARGIN() {
        return 30;
    }

}
