package org.snipereliteteam.snipereliteserver.game.objects;

import org.snipereliteteam.snipereliteserver.Config;
import org.snipereliteteam.snipereliteserver.game.CollisionDetector;

public class Projectile extends GameObject {

    private final OrientationEnum orientation;

    private final int id;

    private final Player player;


    public Projectile(Position position, OrientationEnum orientation, int id, Player player) {
        super(position, Config.BULLET_SIZE, Config.BULLET_SIZE);
        this.orientation = orientation;
        this.id = id;
        this.player = player;
    }


    public void move() {
        switch (orientation) {
            case N:
                position.addY(-25);
                break;
            case S:
                position.addY(25);
                break;
            case E:
                position.addX(25);
                break;
            case W:
                position.addX(-25);
        }
    }


    public boolean hasCollision() {
        return CollisionDetector.checkProjectileCollision(this, player.getSession().getArena().getObjects());
    }


    public Player getPlayerCollision() {
        return CollisionDetector.getPlayerCollisionProjectile(this, player.getSession().getArena().getObjects());
    }


    public int getId() {
        return id;
    }


    public OrientationEnum getOrientation() {
        return orientation;
    }


    public Player getPlayer() {
        return player;
    }

}
