package org.snipereliteteam.snipereliteserver.game.objects;

public enum OrientationEnum {
    N("N"),
    S("S"),
    E("E"),
    W("W");

    private final String orientation;


    OrientationEnum(String orientation) {
        this.orientation = orientation;
    }


    public String getOrientation() {
        return orientation;
    }

}
