package org.snipereliteteam.snipereliteserver.game.objects;

public abstract class GameObject {

    protected Position position;
    protected int width;
    protected int height;


    public GameObject(Position position, int width, int height) {
        this.position = position;
        this.width = width;
        this.height = height;
    }


    public Position getPosition() {
        return position;
    }


    public int getWidth() {
        return width;
    }


    public int getHeight() {
        return height;
    }

}
