package org.snipereliteteam.snipereliteserver.game.objects;

import org.snipereliteteam.snipereliteserver.Config;
import org.snipereliteteam.snipereliteserver.game.Session;
import org.snipereliteteam.snipereliteserver.handler.incoming.MessageHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class Player extends GameObject implements Runnable {

    private final Socket clientSocket;
    private Session session;
    private final int id;

    private OrientationEnum orientation;


    public Player(Socket clientSocket, int id) {
        super(null, Config.PLAYER_SIZE, Config.PLAYER_SIZE);
        this.clientSocket = clientSocket;
        this.id = id;
    }


    @Override
    public void run() {

        BufferedReader in;

        try {

            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            while (!clientSocket.isClosed()) {

                String clientMessage = in.readLine();
                MessageHandler.handleMessage(this, clientMessage);
            }

        } catch (IOException e) {
            System.out.println("Player " + id + " left");
            // TODO: 30/01/2021 Remove player from Arena (if present), Session (if present) and notify Session (if present)
            closeSocket();
        }
    }


    private void closeSocket() {

        try {
            clientSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void addSession(Session session) {
        this.session = session;
    }


    public void setPosition(Position position) {
        this.position = position;
    }


    public Session getSession() {
        return session;
    }


    public Socket getClientSocket() {
        return clientSocket;
    }


    public int getId() {
        return id;
    }


    public OrientationEnum getOrientation() {
        return orientation;
    }


    public void setOrientation(OrientationEnum orientation) {
        this.orientation = orientation;
    }

}
