package org.snipereliteteam.snipereliteserver.game;

import org.snipereliteteam.snipereliteserver.Config;
import org.snipereliteteam.snipereliteserver.factory.ArenaFactory;
import org.snipereliteteam.snipereliteserver.game.arena.Arena;
import org.snipereliteteam.snipereliteserver.game.objects.Player;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class Session {

    private final Arena arena;
    private final List<Player> players = new LinkedList<>();


    public Session() {
        arena = ArenaFactory.createArena();
    }


    public void add(Player player) {
        player.addSession(this);
        players.add(player);
        if (isFull()) {
            arenaInit();
        }
    }


    public boolean isFull() {
        return players.size() == Config.MAX_PLAYERS_SESSION;
    }


    private void arenaInit() {

        try {
            arena.init(players);
        } catch (IOException e) {
            e.printStackTrace();
            // TODO: 23/01/2021 Kick players from Session
        }
    }


    public void removePlayer(Player player) {
        players.remove(player);
        arena.removePlayer(player);
    }


    public List<Player> getPlayers() {
        return players;
    }


    public Arena getArena() {
        return arena;
    }

}
