package org.snipereliteteam.snipereliteserver.game;

import org.snipereliteteam.snipereliteserver.Config;
import org.snipereliteteam.snipereliteserver.game.arena.Arena;
import org.snipereliteteam.snipereliteserver.game.objects.GameObject;
import org.snipereliteteam.snipereliteserver.game.objects.Player;
import org.snipereliteteam.snipereliteserver.game.objects.Position;
import org.snipereliteteam.snipereliteserver.game.objects.Projectile;
import org.snipereliteteam.snipereliteserver.handler.incoming.commands.arguments_enum.MoveEnum;

import java.util.List;

public class CollisionDetector {

    public static boolean changePosition(Player player, MoveEnum move) {

        switch (move) {
            case UP:
                if (!hasArenaCollision(0, -5, player.getPosition(), player.getSession().getArena()) &&
                        !hasObjectCollision(0, -5, player.getPosition(), player.getSession().getArena().getObjects())) {
                    player.getPosition().addY(-5);
                    return true;
                }
                break;
            case DOWN:
                if (!hasArenaCollision(0, 5, player.getPosition(), player.getSession().getArena()) &&
                        !hasObjectCollision(0, 5, player.getPosition(), player.getSession().getArena().getObjects())) {
                    player.getPosition().addY(5);
                    return true;
                }
                break;
            case LEFT:
                if (!hasArenaCollision(-5, 0, player.getPosition(), player.getSession().getArena()) &&
                        !hasObjectCollision(-5, 0, player.getPosition(), player.getSession().getArena().getObjects())) {
                    player.getPosition().addX(-5);
                    return true;
                }
                break;
            case RIGHT:
                if (!hasArenaCollision(5, 0, player.getPosition(), player.getSession().getArena()) &&
                        !hasObjectCollision(5, 0, player.getPosition(), player.getSession().getArena().getObjects())) {
                    player.getPosition().addX(5);
                    return true;
                }
        }
        return false;
    }


    private static boolean hasObjectCollision(int x, int y, Position position, List<GameObject> gameObjects) {

        for (GameObject object : gameObjects) {
            if (object instanceof Player) {
                continue;
            }
            if (position.getX() + x >= object.getPosition().getX() &&
                    position.getX() + x <= object.getPosition().getX() + object.getWidth() &&
                    position.getY() + y >= object.getPosition().getY() &&
                    position.getY() + y <= object.getPosition().getY() + object.getHeight()) {
                return true;
            }
        }
        return false;
    }


    private static boolean hasArenaCollision(int x, int y, Position position, Arena arena) {
        return position.getX() - (Config.PLAYER_SIZE / 2) + x < arena.getMARGIN() ||
                position.getX() + (Config.PLAYER_SIZE / 2) + x > arena.getWIDTH() - arena.getMARGIN() ||
                position.getY() - (Config.PLAYER_SIZE / 2) + y < arena.getMARGIN() ||
                position.getY() + (Config.PLAYER_SIZE / 2) + y > arena.getHEIGHT() - arena.getMARGIN();
    }


    public static boolean checkProjectileCollision(Projectile projectile, List<GameObject> gameObjects) {
        if (projectile.getPosition().getX() < projectile.getPlayer().getSession().getArena().getMARGIN() ||
                projectile.getPosition().getX() > projectile.getPlayer().getSession().getArena().getWIDTH() - projectile.getPlayer().getSession().getArena().getMARGIN() ||
                projectile.getPosition().getY() < projectile.getPlayer().getSession().getArena().getMARGIN() ||
                projectile.getPosition().getY() > projectile.getPlayer().getSession().getArena().getHEIGHT() - projectile.getPlayer().getSession().getArena().getMARGIN()) {
            return true;
        }

        for (GameObject object : gameObjects) {
            if (object == projectile.getPlayer()) {
                continue;
            }
            if (projectile.getPosition().getX() >= object.getPosition().getX() &&
                    projectile.getPosition().getX() <= object.getPosition().getX() + object.getWidth() &&
                    projectile.getPosition().getY() >= object.getPosition().getY() &&
                    projectile.getPosition().getY() <= object.getPosition().getY() + object.getHeight()) {
                return true;
            }
        }
        return false;
    }


    public static Player getPlayerCollisionProjectile(Projectile projectile, List<GameObject> gameObjects) {

        for (GameObject object : gameObjects) {
            if (object == projectile.getPlayer() || !(object instanceof Player)) {
                continue;
            }

            if (projectile.getPosition().getX() >= object.getPosition().getX() &&
                    projectile.getPosition().getX() <= object.getPosition().getX() + object.getWidth() &&
                    projectile.getPosition().getY() >= object.getPosition().getY() &&
                    projectile.getPosition().getY() <= object.getPosition().getY() + object.getHeight()) {
                return (Player) object;
            }
        }

        return null;
    }

}
