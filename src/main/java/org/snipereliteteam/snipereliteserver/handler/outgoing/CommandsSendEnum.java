package org.snipereliteteam.snipereliteserver.handler.outgoing;

public enum CommandsSendEnum {
    ARENA("ARENA"),
    MOVE("MOVE"),
    INIT("INIT"),
    SHOT("SHOT"),
    ADD("ADD"),
    ID("ID"),
    SHOT_R("SHOT_R"),
    KILL("KILL");

    private final String command;


    CommandsSendEnum(String command) {
        this.command = command;
    }

    public String getCommand() {
        return command + "#";
    }

}
