package org.snipereliteteam.snipereliteserver.handler.outgoing;

import org.snipereliteteam.snipereliteserver.Server;
import org.snipereliteteam.snipereliteserver.game.objects.Player;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class MessageDispatcher {

    public static void sendPlayerMessage(Player player, String message) throws IOException {
        PrintWriter out = new PrintWriter(player.getClientSocket().getOutputStream(), true);
        out.println(message);
    }


    public static void sendSessionMessage(List<Player> players, String message) {
        for (Player player : players) {

            try {
                PrintWriter out = new PrintWriter(player.getClientSocket().getOutputStream(), true);
                out.println(message);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public static void sendServerMessage(String message) {

        List<Player> players = Server.getInstance().getPlayers();

        for (Player player : players) {

            try {
                PrintWriter out = new PrintWriter(player.getClientSocket().getOutputStream(), true);
                out.println(message);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
