package org.snipereliteteam.snipereliteserver.handler.incoming.commands.arguments_enum;

public enum MoveEnum {
    UP("UP"),
    DOWN("DOWN"),
    LEFT("LEFT"),
    RIGHT("RIGHT");

    private final String move;

    MoveEnum(String move) {
        this.move = move;
    }


    public String getMove() {
        return move;
    }

}
