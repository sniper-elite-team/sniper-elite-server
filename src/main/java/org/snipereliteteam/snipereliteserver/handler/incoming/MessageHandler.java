package org.snipereliteteam.snipereliteserver.handler.incoming;

import org.snipereliteteam.snipereliteserver.game.objects.Player;
import org.snipereliteteam.snipereliteserver.handler.incoming.commands.ReceiveCommand;
import org.snipereliteteam.snipereliteserver.handler.incoming.commands.ReceiveCommandsEnum;

public class MessageHandler {

    public static void handleMessage(Player player, String clientMessage) {

        String command = clientMessage.substring(0, clientMessage.indexOf("#"));

        String argument = clientMessage.substring(clientMessage.indexOf("#") + 1);

        ReceiveCommand receiveCommand;

        try {
            receiveCommand = ReceiveCommandsEnum.valueOf(command).getCommand();
        } catch (IllegalArgumentException e) {
            receiveCommand = ReceiveCommandsEnum.INVALID.getCommand();
        }
        receiveCommand.handle(player, argument);
    }

}
