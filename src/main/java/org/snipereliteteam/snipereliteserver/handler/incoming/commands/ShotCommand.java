package org.snipereliteteam.snipereliteserver.handler.incoming.commands;

import org.snipereliteteam.snipereliteserver.game.objects.Player;
import org.snipereliteteam.snipereliteserver.game.objects.Position;
import org.snipereliteteam.snipereliteserver.game.objects.Projectile;

public class ShotCommand implements ReceiveCommand {

    public static int ID = 0;


    @Override
    public void handle(Player player, String message) {
        Projectile projectile = new Projectile(new Position(player.getPosition().getX(),
                player.getPosition().getY()),
                player.getOrientation(),
                ID,
                player);
        player.getSession().getArena().addProjectile(projectile);
        ID++;
    }

}
