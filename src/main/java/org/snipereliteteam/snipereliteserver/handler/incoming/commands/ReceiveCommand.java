package org.snipereliteteam.snipereliteserver.handler.incoming.commands;

import org.snipereliteteam.snipereliteserver.game.objects.Player;

public interface ReceiveCommand {

    void handle(Player player, String message);

}
