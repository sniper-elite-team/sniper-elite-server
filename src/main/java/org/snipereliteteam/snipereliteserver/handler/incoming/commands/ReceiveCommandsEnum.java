package org.snipereliteteam.snipereliteserver.handler.incoming.commands;

public enum ReceiveCommandsEnum {

    MOVE(new MoveCommand()),
    SHOT(new ShotCommand()),

    INVALID(new InvalidCommand());

    private final ReceiveCommand command;


    ReceiveCommandsEnum(ReceiveCommand command) {
        this.command = command;
    }


    public ReceiveCommand getCommand() {
        return command;
    }
}
