package org.snipereliteteam.snipereliteserver.handler.incoming.commands;

import org.snipereliteteam.snipereliteserver.game.CollisionDetector;
import org.snipereliteteam.snipereliteserver.game.objects.OrientationEnum;
import org.snipereliteteam.snipereliteserver.game.objects.Player;
import org.snipereliteteam.snipereliteserver.handler.incoming.commands.arguments_enum.MoveEnum;
import org.snipereliteteam.snipereliteserver.handler.outgoing.CommandsSendEnum;
import org.snipereliteteam.snipereliteserver.handler.outgoing.MessageDispatcher;

public class MoveCommand implements ReceiveCommand {

    @Override
    public void handle(Player player, String argument) {

        try {

            MoveEnum movement = MoveEnum.valueOf(argument);

            if (CollisionDetector.changePosition(player, movement)) {

                changeOrientation(player, movement);

                MessageDispatcher.sendSessionMessage(player.getSession().getPlayers()
                        , CommandsSendEnum.MOVE.getCommand() + player.getId() + "#" + player.getPosition().toString() + "#" + player.getOrientation().getOrientation());
            }

        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
        }
    }


    private void changeOrientation(Player player, MoveEnum movement) {
        switch (movement) {
            case UP:
                player.setOrientation(OrientationEnum.N);
                break;
            case DOWN:
                player.setOrientation(OrientationEnum.S);
                break;
            case LEFT:
                player.setOrientation(OrientationEnum.W);
                break;
            case RIGHT:
                player.setOrientation(OrientationEnum.E);
        }
    }

}
