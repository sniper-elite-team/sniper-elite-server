package org.snipereliteteam.snipereliteserver.factory;

import org.snipereliteteam.snipereliteserver.game.arena.Arena;
import org.snipereliteteam.snipereliteserver.game.arena.ArenaEnum;

public class ArenaFactory {

    private ArenaFactory() {
    }


    public static Arena createArena() {
        Arena arena;

        int number = (int) (Math.random() * ArenaEnum.values().length);

        arena = ArenaEnum.values()[number].getArena();

        return arena;
    }

}
