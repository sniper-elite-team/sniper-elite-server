package org.snipereliteteam.snipereliteserver;

public class Config {

    public static final int MAX_PLAYERS_SESSION = 2;
    public static final int MAX_PLAYERS = 30;

    public static final int PLAYER_SIZE = 62;
    public static final int BULLET_SIZE = 3;

}
