package org.snipereliteteam.snipereliteserver;

public class Main {

    public static void main(String[] args) {

        int portNumber = 25566;

        Server server = Server.getInstance();
        server.init(portNumber);
    }

}
