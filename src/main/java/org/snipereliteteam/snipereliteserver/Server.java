package org.snipereliteteam.snipereliteserver;

import org.snipereliteteam.snipereliteserver.game.Session;
import org.snipereliteteam.snipereliteserver.game.objects.Player;
import org.snipereliteteam.snipereliteserver.handler.outgoing.CommandsSendEnum;
import org.snipereliteteam.snipereliteserver.handler.outgoing.MessageDispatcher;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {

    /**
     * Singleton Design Pattern
     */
    private static final Server SERVER = new Server();

    private ServerSocket serverSocket;
    private final List<Session> sessionList = new LinkedList<>();
    private final List<Player> players = new LinkedList<>();

    private int id;


    private Server() {
    }


    public static Server getInstance() {
        return SERVER;
    }


    public void init(int portNumber) {

        try {
            serverSocket = new ServerSocket(portNumber);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        start();
    }


    private void start() {

        Socket clientSocket;
        ExecutorService executorService = Executors.newFixedThreadPool(Config.MAX_PLAYERS);

        while (!serverSocket.isClosed()) {

            try {
                clientSocket = serverSocket.accept();

                Player player = new Player(clientSocket, id);

                players.add(player);

                MessageDispatcher.sendPlayerMessage(player, CommandsSendEnum.ID.getCommand() + id);

                id++;

                executorService.execute(player);
                getSession().add(player);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    private Session getSession() {
        for (Session session : sessionList) {
            if (!session.isFull()) {
                return session;
            }
        }
        Session session = new Session();
        sessionList.add(session);
        return session;
    }


    public void removeSession(Session session) {
        sessionList.remove(session);
    }


    public List<Player> getPlayers() {
        return players;
    }


    public void removePlayer(Player player) {
        players.remove(player);
    }

}
